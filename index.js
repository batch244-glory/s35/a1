// on terminal a1 folder
// npm init -y
// npm install express mongoose


/*
Activity:
In s35 > a1 folder,
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S35.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.

*/
const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.pqfdnli.mongodb.net/b244-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", ()=>{console.log("We're connected to the database")});

const userSchema = new mongoose.Schema({
	name: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}, (err, res) => {
		if (result != null && result.username == req.body.username){
			return res.send("Duplicate username found");
		} else {
			if (req.body.username !== "" && req.body.password !== ""){
				let newUser = new User({
					username : req.body.username,
					password : req.body.password
				})

				newUser.save((saveErr, savedTask) => {
					if(saveErr){
						return console.error(saveErr)
					} else {
						return res.status(201).send("User registered successfully")
					}
				})
			}
		}
	})
})

app.listen(port, () => {console.log(`Server running at port ${port}`)});